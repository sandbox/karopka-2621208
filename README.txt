INTRODUCTION
------------

The Webform Select Or Other Options module extends a "select or other field" on a webform by adding ability to reuse "other" text.
After submiting webform adds "Other" option into the database, 
so when you launch your webform again new option will be in the default list of options.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/karopka/2621208

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2621208
   
REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://drupal.org/project/webform)
 
 INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------

Current maintainers:
 * Alexey A. Tseluiko (karopka) - https://www.drupal.org/u/karopka
