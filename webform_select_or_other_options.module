<?php
/**
 * @file
 * Adds a user-entered text in the Other to the list of default options.
 */

/**
 * Constants.
 */
define('WEBFORM_SELECT_OR_OTHER_OPTIONS_OPTIONS', 'webform_select_or_other_option_options');

/**
 * Implements hook_webform_submission_presave().
 */
function webform_select_or_other_options_webform_submission_presave($node, $submission) {
  webform_select_or_other_options_add_other_option($node, $submission);
}

/**
 * Implements hook_webform_submission_update().
 */
function webform_select_or_other_options_webform_submission_update($node, $submission) {
  webform_select_or_other_options_add_other_option($node, $submission);
}

/**
 * Save other option to database on webform save/update.
 *
 * @param object $node
 *   Node with webform.
 * @param object $submission
 *   Submission of current webform.
 */
function webform_select_or_other_options_add_other_option($node, $submission) {
  // Find select or other component, research options and udpate if needed.
  $components = $node->webform['components'];
  foreach ($components as $i => $component) {
    $type = $component['type'];
    if ($type == 'select') {
      $string = $component['extra']['items'];
      $cid = $component['cid'];
      $nid = $component['nid'];
      $sid = $submission->sid;
      $options = webform_select_or_other_options_string_to_array($string);
      // Check for OTHER option and add it to default list.
      $isnum = intval($submission->data[$i][0]);
      $len = strlen($submission->data[$i][0]);
      if (!$isnum && $len > 0) {
        $count = count($options);
        if ($submission->data[$i][0] <> '0') {
          $options[$count] = $submission->data[$i][0];
          $oid = $count;
          variable_set(WEBFORM_SELECT_OR_OTHER_OPTIONS_OPTIONS, $options);
          $result = webform_select_or_other_options_array_to_string($options);
          // Update db table "webform_component".
          webform_select_or_other_options_update_webform_component($nid, $cid, $result);
          // Update db table "webform_submissions".
          webform_select_or_other_options_update_webform_submissions($nid, $cid, $sid, $oid);
        }
      }
    }
  }
}

/**
 * Get array from string.
 *
 * @param string $str
 *   Serialized string.
 *
 * @return array
 *   Array from unserialized string.
 */
function webform_select_or_other_options_string_to_array($str) {
  $results = array(0);
  $rows = explode("\n", $str);
  foreach ($rows as $i => $row) {
    $str = explode("|", $row);
    if (count($str) == 2) {
      $results[$i] = $str[1];
    }
  }
  return $results;
}

/**
 * Get string from array.
 *
 * @param array $array
 *   Array with other option.
 *
 * @return string
 *   Unserialized string.
 */
function webform_select_or_other_options_array_to_string(array $array) {
  $arr = array(0);
  foreach ($array as $i => $item) {
    $arr[$i] = $i . '|' . $item;
  }
  $string = implode("\n", $arr);
  return $string;
}

/**
 * Update DB table webform_component.
 *
 * @param int $nid
 *   Id of current node.
 * @param int $cid
 *   Id of the component in a webform.
 * @param string $new_data
 *   New list of options (with other).
 *
 * @return bool
 *   Return True.
 */
function webform_select_or_other_options_update_webform_component($nid, $cid, $new_data) {
  // Get webformn component.
  $array = db_select('webform_component', 'w')
    ->fields('w', array('extra'))
    ->condition('w.nid', $nid)
    ->condition('w.cid', $cid)
    ->execute()
    ->fetchAll();
  $extra = unserialize($array[0]->extra);
  $extra['items'] = $new_data;
  $result = serialize($extra);
  // Write new options to Select list.
  db_update('webform_component')
    ->fields(array('extra' => $result))
    ->condition('nid', $nid)
    ->condition('cid', $cid)
    ->execute();
  return TRUE;
}

/**
 * Update DB table webform_component.
 *
 * @param int $nid
 *   Id of current node.
 * @param int $cid
 *   Id of the component in a webform.
 * @param int $sid
 *   Id of the submission.
 * @param int $option_id
 *   Id of new option.
 *
 * @return bool
 *   Return true.
 */
function webform_select_or_other_options_update_webform_submissions($nid, $cid, $sid, $option_id) {
  db_update('webform_submitted_data')
    ->fields(array('data' => $option_id))
    ->condition('nid', $nid)
    ->condition('cid', $cid)
    ->condition('sid', $sid)
    ->execute();
  return TRUE;
}
